

//App page - https://lesson-9-classwork.herokuapp.com/newpage


import React from 'react';
import { BrowserRouter, Route, Link} from 'react-router-dom';
import { Provider } from 'react-redux';


import store from './redux/store';
import AllTasks from './components/allTaks';
import UndoneTask from './components/undoneTask';
import DoneTask from './components/doneTask';

import Context from './components/context';
import Page2 from './components/page2';
import withHelmet from './components/withHelmet';

const WrappedAllTasks = withHelmet({title:'Tasks'})(AllTasks)
const WrappedDoneTask = withHelmet({title:'Done Tasks'})(DoneTask)
const WrappedUndoneTask = withHelmet({title:'Undone Tasks'})(UndoneTask)

class App extends React.Component {
  render = () => {
    return (
      <BrowserRouter>
        <Context>
          <Provider store={store}>
              <Link to='/'> Main </Link>
              <Link to='/done'> Done </Link>
              <Link to='/undone'> Undone </Link>
              <Link to='/newpage'> NewPage </Link>

              <Route exact path='/' component={WrappedAllTasks}/>
              <Route exact path='/done' component={WrappedDoneTask}/>
              <Route exact path='/undone' component={WrappedUndoneTask}/>
              <Route exact path='/newpage' component={Page2}/>
          </Provider>
        </Context>
      </BrowserRouter>
    );
  }
}

export default App;
