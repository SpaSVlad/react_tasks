import React from 'react';
import List from './list'
import { connect } from 'react-redux';

class AllTasks extends React.Component {

	state = {
		newTask: { 
		}
	}

	addTask = (e) => {
		let inputValue = e.target.value;

		this.setState({
			newTask: {done:false, name:inputValue, id: new Date().getTime()}
		})
	}

	addTaskToList = (e) => {
		e.preventDefault()
		const {addTaskToStore} = this.props;
		const {newTask} = this.state;

		if (newTask.name !== '') {
			addTaskToStore(newTask)
		}
		e.target.elements.input.value = ''
	}

	render = () => {
		const {addTask, addTaskToList} = this;
		return (
				<div>
					<h1>List</h1>
					<form onSubmit={addTaskToList}>
						<input onChange={addTask} name='input'></input>
						<button>Add Task</button>
					</form>
					<List/>
				</div>
		)
	}
}

const mapStateToProps = ( state ) => {
	return {
		tasks:state.tasks
	}
}

const mapDispatchToProps = ( dispatch ) => ({
	
		addTaskToStore: (task) => {
			dispatch({
				type:'ADD',
				payload: task
			})
		}
})

export default connect(mapStateToProps, mapDispatchToProps)(AllTasks);