import React from 'react';
import Helmet from 'react-helmet';

import favicon from '../images/favicon_home.ico?v2';


const withHelmet = (params) => (Component) => {
	console.log(params)
	return () => (
			<>
				<Helmet>
					<title>{params.title}</title>
					<link rel="shortcut icon" href={favicon}/>
				</Helmet>
				<Component/>
			</>		
		)
}

export default withHelmet;
