import React from 'react';
import { connect } from 'react-redux';

class UndoneTask extends React.Component {

	render = () => {
		const {tasks} = this.props;
		return (
			<ul>
				{
					tasks.map(task => {
						return !task.done ? <li key={task.id}><h3>{task.name}</h3></li> : null
					})
				}
			</ul>
		)			
	}
}

const mapStateToProps = ( state ) => {
	return {
		tasks:state.tasks
	}
}
export default connect(mapStateToProps, null)(UndoneTask);
