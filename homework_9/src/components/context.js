import React from 'react';

export const Context = React.createContext();

class ContextComponent extends React.Component {

	render = () => {
		const { children } = this.props;
		return (
				<Context.Provider value={{hello:'hello'}}>
					{ children }
				</Context.Provider>
			)
	}

}

export default ContextComponent;