import React from 'react';
import { connect } from 'react-redux';

class List extends React.Component {

	doneTask = (e) => {
		let currentElemId = e.target.dataset.id;
		const {tasks, markAsDone} = this.props;

		let changeTasks = tasks.map(task => {
			if (task.id === Number(currentElemId)) {
				task.done = true;
			}
			return task
		});
		markAsDone(changeTasks);
	}

	removeTask = (e) => {
		let currentElemId = e.target.dataset.id;
		const {tasks, removeTaskFromStore} = this.props;

		let filteredArr = tasks.filter(task => {
		  return Number(currentElemId) === task.id ? null : task
		});

		removeTaskFromStore(filteredArr)
	}

	render = () => {
		const {tasks} = this.props;
		const { doneTask, removeTask} = this;
		return (
				<ul> 
					{
						tasks.map( task => {
							return (
								<li key={task.id} style={{color:task.done ? '#00ab04' : '#e72e2e'}}> 
									<h3>{task.name}</h3>
									<button onClick={removeTask} data-id={task.id}>Remove</button>
									<button onClick={doneTask} data-id={task.id}>Done</button>
								</li>	
							)
						})
					}
				</ul>
			)
	}
}

const mapStateToProps = ( state ) => {
	return {
		tasks:state.tasks
	}
}

const mapDispatchToProps = ( dispatch ) => ({

		markAsDone: (task) => {
			dispatch({
				type:'DONE',
				payload:task
			})
		},

		removeTaskFromStore: (task) => {
			dispatch({
				type:'REMOVE',
				payload:task
			})
		}

})

export default connect(mapStateToProps, mapDispatchToProps)(List);