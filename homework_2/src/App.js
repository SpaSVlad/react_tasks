import React from 'react';
import User from './classWork/user';

import Error from './homework/task1/error';
import ImageComponent from './homework/task1/imageComponent';

import Table from './homework/task3/table';

export class AppClassWork extends React.Component {

  state = {
    data: []
  }

  buttonFunc = (e) => {
      let indexElement = Number(e.target.dataset.index);
      const interviewedUsers = this.state.data.map(item => {
        if (item.index === indexElement) {
          item.interviewed = !item.interviewed
        }
          return item
      })
      this.setState({
        data:interviewedUsers
      })
  }

  componentDidMount = () =>  {
    fetch(`http://www.json-generator.com/api/json/get/cqNPvAmeRe?indent=2`)
      .then(res => res.json())
      .then( res => {
        let usersArray = res.map (item => {
            return {
                interviewed:false,
                user:item.name,
                index:item.index
            }
        })
        this.setState({
          data:usersArray
        })
        console.log(this.state)
      })
  }

  render () {
    const {buttonFunc} = this;
    const {data} = this.state;
    return (
          <div>
            <ul>
                  {
                    data.map ((item,key) => (
                      <User 
                        name = {item.user} 
                        key ={key}
                        func = {buttonFunc}
                        index ={item.index}
                        checked={item.interviewed}/>
                    ))     
                  }
            </ul>
          </div>
        ) 
      }
}

export class AppHomework extends React.Component {

  render = () => {
    return (
        <div>
          <Error>
            <ImageComponent src='http://goodimg.ru/img/chernaya-kartinka2.jpg'/>
          </Error>
          <Table/>
        </div>
      )
  }
}


