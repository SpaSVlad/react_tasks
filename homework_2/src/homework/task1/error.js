import React from 'react';

class Error extends React.Component {

	state = {
		error: false
	}
	
	componentDidCatch( error, info ){
 
        this.setState ({
        	error:true
        })
    }

	render = () => {
		const{error} = this.state;
		const{children} = this.props;

		if (error) {
			return (
					<div>Some error (</div>
				)
		}
		return (
				<div>{children}</div>
			)
	}
}
export default Error;