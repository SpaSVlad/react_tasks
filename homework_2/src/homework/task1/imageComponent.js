import React from 'react';

class ImageComponent extends React.Component {

	state = {
		loaded: false,
	}

	componentDidMount (){

		this.setState({
			loaded: true
		})
	}

	render = () => {
		const {loaded} = this.state;
		const {src} = this.props;
		if (!loaded) {
			return (
					<h1>Loading...</h1>
			)
		} 
		return (
			<img src={src} style={{width:'300px',height:'200px'}}></img>
		)
	}
}
export default ImageComponent;