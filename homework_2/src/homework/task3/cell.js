import React from 'react';

const Cell = ({value, colspan, color, background, type, currency}) => {
	let typeElement = type.toUpperCase().toString();

	if (typeElement === 'DATE') {
		return (
			<td 
				colSpan = {colspan}
				style ={{
					color:color,
					background:background,
					fontStyle: 'italic'
			}}>

				{value}</td>
		)	
	}
	if (typeElement === 'NUMBER') {
		return (
			<td 
				colSpan = {colspan}
				style ={{
					color:color,
					background:background,
					textAlign:'right'
			}}>

				{value}</td>
			)	
	}
	if (typeElement === 'MONEY') {
		return ( <td 
				colSpan = {colspan}
				style ={{
					color:color,
					background:background,
					textAlign:'right'
			}}>

				{value +currency}</td>
			)	
	}
	if (typeElement === 'TEXT') {
		return (
			<td 
				colSpan = {colspan}
				style ={{
					color:color,
					background:background,
					textAlign:'left'
			}}>

				{value}</td>
			)	
	}
	return (
			<td 
				colSpan = {colspan}
				style ={{
					color:color,
					background:background,
			}}>
				{value}</td>
		)
}

Cell.defaultProps = {
	type: 'text',
	value: '',
	colspan: 1,
	color: '#000',
	background:'transparent', 
	currency:'$'
}
export default Cell;