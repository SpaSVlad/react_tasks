import React from 'react';
import Row from './row';
import Cell from './cell';

class Table extends React.Component {

	render = () => {
		return (
			<table>
				<Row>
					<Cell value = {3} colspan ={3} color= 'red' background = 'green' type = 'money'/>
					<Cell value = '2'/>
					<Cell value = 'we' type = 'number'/>
				</Row>
				<Row head = {true}>
					<Cell value = 'we' colspan ={2} color= 'blue'/>
					<Cell value = 'alll' background = 'yellow' type = 'text'/>
					<Cell value =  {12} type = 'date'/>
				</Row>
				<Row head >
					<Cell value = 'alll' background = 'yellow' type = 'money'/>
					<Cell value =  {12} type = 'date'/>
					<Cell value = 'yellow' colspan ={4} color= 'red'/>
				</Row>
				<Row head >
					<Cell value = 'alll' background = 'yellow' type = 'money'/>
					<Cell value = 'we' colspan ={5} color= 'blue'/>
					<Cell value =  {12} type = 'date'/>
				</Row>
			</table>
		)
	}
}

export default Table;