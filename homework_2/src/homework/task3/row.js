import React from 'react';

const Row = ({children, head}) => {
	if (head) { 
		return (
			<thead> 
				<tr>
					{children}
				</tr>
			</thead>
		)
	}
	return (
		<tbody>
			<tr>
				{children}
			</tr>
		</tbody>
		)
}
Row.defaultProps = {
	head:false
}
export default Row;