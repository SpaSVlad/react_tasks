import React from 'react';

const styleButton = {
	border:'none',
	borderRadius:'3px',
	padding:'10px',
	textTransform: 'uppercase',
	color:'#fff',
	background:'blue'

}


const Button = ({func, index}) => {
	return (
		<button style ={styleButton} onClick = {func} data-index ={index}>Press me</button>
	)
}

export default Button;