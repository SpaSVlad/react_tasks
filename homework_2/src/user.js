import React from 'react';
import Button from './button';

const User = ({name, func, index, checked}) => {
	return (
		<li style = {{
			background:checked ? 'green':'red',
			margin:'10px 0px',
			listStyle:'none',
			display:'flex',
			justifyContent:'space-between'}}>
			{name}
			<Button func ={func} index= {index}/>
		</li>
	)
}

export default User;