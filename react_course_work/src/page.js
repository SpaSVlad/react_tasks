import React from 'react';

class Page extends React.Component {

	state = {
		popular:[
		]
	}

	componentDidMount = () => {
		fetch('https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=593be2335a3223904f7ba52d3387fe16')
		.then(res => res.json())
		.then(res => {
			console.log(res)
			this.setState({
				popular: res.results
			})

		})
	}

	render = () => {
		console.log(this.state)
		return (
				<div>
					<ul>
						{
							this.state.popular.map(item => {
								return (
									<li key={item.id}>
										<img src={`https://image.tmdb.org/t/p/w500${item.poster_path}`}></img>
										<h1>{item.title}</h1>
									</li>
									)
							})
						}
					</ul>
				</div>
			)
	}	
}
export default Page;