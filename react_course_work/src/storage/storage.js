import store from '../redux/store';

export function loadFromStorage(id){
	let data = localStorage.getItem(`${id}`)
	let parsedData = JSON.parse(data);
	store.dispatch({
		type:'LOAD_FROM_STORE',
		payload: parsedData
	})
}

export function saveToStorage(obj){
	const strData = JSON.stringify(obj)
	localStorage.setItem(`${obj.details.id}`, strData)
}

export function checkStorage(id) {
	const data = localStorage.getItem(id)
	if (data === null) {
		return true
	} else {
		loadFromStorage(id)
		return false
	}
}

