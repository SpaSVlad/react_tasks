const initState = {
	popular:[]
}

const popularMovies = (state = initState, action) => {
		switch(action.type){
			case 'GET_POPULAR_MOVIES' :
				return { 
					loaded:true,
					popular:[
						...state.popular,
						...action.payload
					]
				}

			default: 
				return state
		}

}  

export default popularMovies;