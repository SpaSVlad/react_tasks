import { combineReducers } from 'redux';
import upcomingMovies from './upcomingMovies';
import genresState from './genres';
import popularMovies from './popular';
import movieDetails from './movieDatails';


const reducer = combineReducers({
	upcomingMovies,
	genresState,
	popularMovies,
	movieDetails
})

export default reducer;


 


