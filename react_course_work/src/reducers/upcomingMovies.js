const initState = {
	loaded:false,
	movies:[]
}

const upcomingMovies = (state = initState, action) => {
		switch(action.type){
			case 'GET_UPCOMING_MOVIES' :
				return { 
					loaded:true,
					movies:[
						...state.movies,
						...action.payload
					]
				}

			default: 
				return state
		}

}  

export default upcomingMovies;