const initState = {
	loaded:false,
	genres:[]
}

const genresState = (state = initState, action) => {
		switch(action.type){
			case 'GET_GENRES' :
				return { 
					loaded:true,
					genres:[...action.payload]
				}

			default: 
				return state
		}

}  

export default genresState;