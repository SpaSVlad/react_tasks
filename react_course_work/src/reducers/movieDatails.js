const initState = {
	details:{},
	similar:[],
	trailers_data:[],
	error:false
}

const movieDetails = (state = initState, action) => {
		switch(action.type){
			case 'GET_MOVIE_DETAILS' :
				return { 
					...state,
					details:{...action.payload},
					error:false
				}
			case 'GET_SIMILAR_MOVIES' :
				return { 
					...state,
					similar:[...action.payload],
					error:false
				}
			case 'GET_MOVIE_TRAILERS': 
				return {
					...state,
					trailers_data:[...action.payload],
					error:false
				}
			case 'RATE_MOVIE': 
				return {
					...state,
					details:{
						...state.details,
						mark:action.payload
					}
				}
			case 'LOAD_FROM_STORE':
				return {
					...action.payload
				}
			case 'GET_ERROR': 
				return {
					...state,
					error:true
				}
			default: 
				return state
		}

}  

export default movieDetails;