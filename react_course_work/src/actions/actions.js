export const getUpcomingMovies = (num) => (dispatch, getState) => {
	fetch(`https://api.themoviedb.org/3/movie/upcoming?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US&page=${num}`)
		.then(res => res.json())
		.then(res => {
			dispatch({
				type: 'GET_UPCOMING_MOVIES',
				payload: res.results
			})
		})
}

export const getPopularMovies = (num) => (dispatch, getState) => {
	fetch(`https://api.themoviedb.org/3/movie/popular?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US&page=${num}`)
		.then(res => res.json())
		.then(res => {
			dispatch({
				type: 'GET_POPULAR_MOVIES',
				payload: res.results
			})
		})
}


export const getGenres = () => (dispatch, getState) => {
	fetch('https://api.themoviedb.org/3/genre/movie/list?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US')
		.then(res => res.json())
		.then(res => {
			dispatch({
				type: 'GET_GENRES',
				payload: res.genres
			})
		})
}

export const getDetailsMovie = (id) => (dispatch, getState) => {
		fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US`)
		.then(res => res.json())
		.then(res => {
			dispatch({
				type: 'GET_MOVIE_DETAILS',
				payload: res
			})
		})	
		.catch(err => {
			dispatch({
				type: 'GET_ERROR'
			})
		})
}

export const getSimilarMovies = (id) => (dispatch, getState) => {
		fetch(`https://api.themoviedb.org/3/movie/${id}/similar?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US&page=1`)
		.then(res => res.json())
		.then(res => {
			dispatch({
				type: 'GET_SIMILAR_MOVIES',
				payload: res.results
			})
		})
		.catch(err => {
			dispatch({
				type: 'GET_ERROR'
			})
		})
}
export const getMovieTrailers = (id) => (dispatch, getState) => {
		fetch(`https://api.themoviedb.org/3/movie/${id}/videos?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US`)
		.then(res => res.json())
		.then(res => {
			dispatch({
				type: 'GET_MOVIE_TRAILERS',
				payload: res.results
			})
		})
		.catch(err => {
			dispatch({
				type: 'GET_ERROR'
			})
		})
}

export const rateFilm = (mark) => (dispatch, getState) => {
		dispatch({
				type: 'RATE_MOVIE',
				payload: mark
			})
}


