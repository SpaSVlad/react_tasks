import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducers/index';

import { saveToStorage } from '../storage/storage';

const composeEnhancers = process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

const middleware = applyMiddleware(thunk)

let store = createStore(reducer, composeEnhancers(middleware));


store.subscribe(() => {
	let state = store.getState();
	let movie_details = state.movieDetails;
	
	if (movie_details.details.id !== undefined) {
		saveToStorage(movie_details)
	}
})



export default store;


