import React from 'react';
import { connect } from 'react-redux';
import { Route, Link} from 'react-router-dom';

import { getGenres } from '../actions/actions';
import MoviesGenres from './moviesByGenres';


class Catalog extends React.Component {

	componentDidMount = () => {
		this.props.getAllGenres()
	}

	render = () => {
		const { state } = this.props;
		return (
				<div className='genres'>
					<div className='genres_list'>
						<h1 className='genres_list_title'>Genres</h1>
						<ul className='genres_list_inner'>
							{
								state.genres.map(item => {
									return (
											<li key={item.id}><Link key={item.id}  to={`/catalog/genres/${item.id}`}>{item.name}</Link></li>
										)
								})
							}
						</ul>
					</div>
					<Route exact path='/catalog/genres/:id' component={MoviesGenres}/>
				</div>
			)
	}
}

const mapStateToProps = ( state) => {
    return {
       state:state.genresState,
       loaded:state.loaded
    }
};

const mapDispatchToProps = ( dispatch) => ({
    getAllGenres: () => {
        dispatch( getGenres () )
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);



