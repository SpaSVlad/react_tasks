import React from 'react';
import { Route, Switch, NavLink, Link} from 'react-router-dom';


import Main from './mainPage';
import Popular from './popular';
import Catalog from './catalog';
import Error from './error';
import MoviePage from './movie_page.components/moviePage';
import withHelmet from './withHelmet';
import SearchResults from './searchResultPage';



const wrappedMain = withHelmet({title:'Upcoming Movies'})(Main);
const wrappedPopular = withHelmet({title:'Popular Movies'})(Popular);
const wrappedCatalog = withHelmet({title:'Catalog'})(Catalog);  
const wrappedSearchResults = withHelmet({title:'Search Results'})(SearchResults); 
const wrappedMoviePage = withHelmet({title:'Movie'})(MoviePage); 


class Container extends React.Component {
	state = {
		value:''
	}

	searchValue = (e) => {
		this.setState({
			...this.state,
			value:e.target.value
		})
	}

	render = () => {
		const { searchValue } = this;
		const { value } = this.state;

		return (	
				<div className='main'>
					<div className='header'>
						<div className='navigation'>
							<NavLink to='/'> Main </NavLink>
							<NavLink to='/popular'> Popular </NavLink>
							<NavLink to='/catalog'> Catalog </NavLink>
						</div>
						<div className='search_block'>
							<input placeholder='Search movie' onChange={searchValue}></input>

							<Link to={{	pathname:`/search/${value}`}}>Search</Link>
						</div>
					</div>
					<Switch>
						<Route exact path='/' component={wrappedMain}/>
						<Route exact path='/popular' component={wrappedPopular}/>
						<Route path='/catalog' component={wrappedCatalog}/>
						<Route exact path='/movie/:idmovie' component={wrappedMoviePage}/>
						<Route path='/search/:str' component={wrappedSearchResults}/>
						<Route component={Error}/>
					</Switch>
				</div>
			)
	}

}

export default Container;