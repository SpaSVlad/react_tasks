import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { getPopularMovies } from '../actions/actions';
import star from '../images/favourites.png';


class Popular extends React.Component {

	state = {
		page:1
	}

	showMore = () => {
		const { page } = this.state;
		this.props.getMovies( page+1 )
		this.setState({
			page:this.state.page+1
		})
	}

	componentDidMount = () => {
		const { page } = this.state;
		this.props.getMovies( page )
	}

	render = () => {
		const { popular } = this.props.state;
		const { showMore } = this;
		
		return (
				<ul className='movies_list'>
					{
						popular.map( (item, index) => {
							let date = new Date(Date.parse(item.release_date));
							return (
								<Link key={index} to={`/movie/${item.id}`}>
									<h1>{item.title}</h1>
									<img src={`https://image.tmdb.org/t/p/w300${item.poster_path}` } alt='movies_image'></img>
									<p>Avarage mark: <span>{item.vote_average}</span> <img src={star} alt='star'></img></p>
									<p>Release: <span>{date.toDateString()}</span></p>
								</Link>
								)
						})
					}
					<button onClick={showMore} >Show more...</button>
				</ul>
			)
	}
}
const mapStateToProps = ( state ) => {
    return {
    	state: state.popularMovies
    }
};

const mapDispatchToProps = ( dispatch) => ({
    getMovies: (num) => {
    	dispatch( getPopularMovies(num) )
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Popular);