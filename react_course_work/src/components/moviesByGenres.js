import React from 'react';
import { Link } from 'react-router-dom';

class MoviesGenres extends React.Component {

	state={
		current:[],
		page:1
	}

	componentDidMount = () => {
		const { params } = this.props.match;
		fetch(`https://api.themoviedb.org/3/discover/movie?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${params.id}`)
		.then(res => res.json())
		.then(res => {
			this.setState({
				current:res.results
			})
		})
	}

	componentDidUpdate = (prevProps, prevState) => {
		if (prevProps.match.params.id !== this.props.match.params.id) {
		  	const { params } = this.props.match;
			fetch(`https://api.themoviedb.org/3/discover/movie?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${params.id}`)
			.then(res => res.json())
			.then(res => {
				this.setState({
					current:res.results,
					page:1
				})
			})
		} else {
			return 
		}
	}

	showMore = () => {
		const { params } = this.props.match;
		const { page } = this.state;
		fetch(`https://api.themoviedb.org/3/discover/movie?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${page+1}&with_genres=${params.id}`)
		.then(res => res.json())
		.then(res => {
			this.setState({
				current:[
					...this.state.current,
					...res.results
				],
				page:this.state.page+1
			})
		})
	}


	render = () => {
		const { current } = this.state;
		const { showMore } = this;
		return (
				<div>
					<ul className='genres_movies_list_inner'>
						{		
							current.map((item, index) => {
								return (
									<Link key={index} to={`/movie/${item.id}`}>
										<h3>{item.title}</h3>
										<img src={`https://image.tmdb.org/t/p/w300${item.poster_path}`} alt='movies_image'></img>
									</Link>
									)
								})
						}
					<button onClick={showMore}>Show more... </button>
					</ul>
				</div>
			)

	}
}

export default MoviesGenres;