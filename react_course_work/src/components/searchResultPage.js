import React from 'react';
import { Link } from 'react-router-dom';
import default_poster from '../images/default-movie.jpg';

class SearchResults extends React.Component {

	state = {
		value: this.props.props.match.params.str,
		searchData:[]
	}

	componentDidMount = () => {
		let { str } = this.props.props.match.params;

		let newStr = str.replace(/\s/g, '');

		fetch(`https://api.themoviedb.org/3/search/movie?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US&query=${newStr}&page=1&include_adult=false`)
	 	.then(res => res.json())
	 	.then(res => {
	 		this.setState({
	 			value:str,
	 			searchData:res.results
	 		})
	 	})
	}

	componentDidUpdate = (prevProps, prevState) => {
		const prevStr  = prevProps.props.match.params.str;
		let { str } = this.props.props.match.params;

		if (prevStr !== str) {
			let newStr = str.replace(/\s/g, '');
			fetch(`https://api.themoviedb.org/3/search/movie?api_key=593be2335a3223904f7ba52d3387fe16&language=en-US&query=${newStr}&page=1&include_adult=false`)
		 	.then(res => res.json())
		 	.then(res => {
		 		this.setState({
		 			value: str,
		 			searchData:res.results
		 		})
		 	})
		}
	}

	render = () => {
		const { searchData, value } = this.state;
		return (
				<div className='genres_movies_list_outer'>	
					<h2>All search results: "{value}"</h2>
					<ul className='genres_movies_list_inner'>
						{
							searchData.map((item, index) => {
								return (
									<Link key={index} to={`/movie/${item.id}`}> 
										<h3>{item.title}</h3>
										<img src={item.poster_path != null ? `https://image.tmdb.org/t/p/w300${item.poster_path}` : default_poster} 
											 alt='movies_image'>
										</img>
									</Link>
									)
							})
						}
					</ul>			
				</div>
			)
	}
}

export default SearchResults;