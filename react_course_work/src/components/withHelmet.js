import React from 'react';
import Helmet from 'react-helmet';
import favicon from '../images/cinema.ico?v2'

const withHelmet = ({title}) => (Component) =>{
	
	return (props) => (
		<div className='helmet_wrapper'>
			<Helmet>
				<title>{title}</title>
				<link rel="shortcut icon" href={favicon}/>
			</Helmet>
			<Component props={{...props}}/>
		</div>
	)

}

export default withHelmet;