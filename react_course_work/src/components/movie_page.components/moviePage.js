import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import SimilarMovies from './similarMovies';
import MovieGenres from './movieGenres';
import TrailerLinks from './trailers';
import RateMovie from './rateMovie';
import { getDetailsMovie, 
		 getSimilarMovies,
		 getMovieTrailers,
		 rateFilm } from '../../actions/actions';

import { checkStorage  } from '../../storage/storage'; 

import star from '../../images/favourites.png';
import dafault_poster from '../../images/default-movie.jpg'


class Movie extends React.Component {

	state = {
		showSimilar:false
	}

	componentDidMount = () => {	
		const {	idmovie } = this.props.props.match.params;
		if (checkStorage(idmovie)) {
			this.props.getDetails(idmovie)
			this.props.getSimilar(idmovie)
			this.props.getTrailers(idmovie)	
		} 
	}

	componentDidUpdate = (prevProps, prevState) => {
		const {	idmovie } = this.props.props.match.params;

		if (idmovie !== prevProps.props.match.params.idmovie) {
			if (checkStorage(idmovie)) {
				this.props.getDetails(idmovie)
				this.props.getSimilar(idmovie)
				this.props.getTrailers(idmovie)
			} 
		}
	}

	changeStatus = () => {
		this.setState({
			...this.state,
			showSimilar: !this.state.showSimilar
		})
	}

	rateMovie = (e) => {
		this.props.moviesMark(e.target.value);
		/*saveToStorage(this.props.state)*/
	}

	render = () => {
		const { showSimilar} = this.state;
		const { changeStatus, rateMovie } = this;
		const { details, similar, error, trailers_data } = this.props.state;

		return (
				<div className='movie_page'>
					{
						error ? (
									<Redirect to='/error'/>
								) : 
								(
								<div className='movie_page_content'>
							 		<div className='movie_poster'>
							 			{ 
							 				details.poster_path !== null ? (
							 					<img src={`https://image.tmdb.org/t/p/w300${details.poster_path}`} alt='movies_image'></img>
											) :
											(
							 					<img src={dafault_poster} alt='movies_image'></img>
											)
							 			}
							 		</div>
							 		<div className='movie_data'>
								 		<h1>{details.original_title}</h1>	
								 		<h3>{details.tagline}</h3>
								 		<ul className='data_list'>
									 		<li><span>Popularity:</span> <p>{details.popularity}</p></li>
									 		<li><span>Status:</span> <p>{details.status}</p></li>
									 		<li><span>Revenue:</span> <p>{details.revenue} $</p></li>
									 		<li><span>Release Date:</span> <p>{details.release_date}</p></li>
									 		<li className='rating'>
									 			<span>Average rating:</span> 
									 			<p>{details.vote_average}
									 				<img src={star} alt='star'></img>
									 			</p>
									 		</li>
									 		<li><p>{details.overview}</p></li>
								 		</ul>
									 	<>
									 		<TrailerLinks props={trailers_data}/>
									 	</>
								 		<>
								 			<MovieGenres props={details.genres}/>
								 		</>
								 		<>
								 			<RateMovie handler={rateMovie} state={{...details}}/>
								 		</>
								 		<button onClick={changeStatus} className='show_similar'>Show similar movies</button>
								 		<>
									 		{
									 			showSimilar ? (<SimilarMovies props={similar}/>) : null
									 		}
								 		</>
							 		</div>
							 	</div>
					 		)
					}
				</div>
			)
	}
}

const mapStateToProps = ( state ) => {
    return {
    	state: state.movieDetails
    }
};

const mapDispatchToProps = ( dispatch) => ({
    getDetails: (num) => {
    	dispatch( getDetailsMovie(num) )
    },
    getSimilar: ( num ) => {
    	dispatch( getSimilarMovies(num))
    },
    getTrailers: ( num ) => {
    	dispatch( getMovieTrailers(num))
    },
    moviesMark: ( num ) => {
    	dispatch( rateFilm(num))
    }
});


export default connect(mapStateToProps, mapDispatchToProps)(Movie);
