import React from 'react';
import star from '../../images/favourites.png';

const MovieGenres = ( { handler, state } ) => {
	return (
			<div className='rating_block'>
				<div className='rating_block_inner'>
					<h3> Rate the movie:</h3>
					<label htmlFor='mark_1'>1
						<input type='radio' 
							   name='movie_rate' 
							   value='1' 
							   onClick={handler} 
							   id='mark_1'>
						</input>
					</label>
					<label htmlFor='mark_2'>2
						<input type='radio' 
							   name='movie_rate' 
							   value='2' 
							   onClick={handler} 
							   id='mark_2'>
						</input>
					</label>
					<label htmlFor='mark_3'>3
						<input type='radio' 
							   name='movie_rate' 
							   value='3' 
							   onClick={handler} 
							   id='mark_3'>
						</input>
					</label>
					<label htmlFor='mark_4'>4
						<input type='radio' 
							   name='movie_rate' 
							   value='4' 
							   onClick={handler} 
							   id='mark_4'>
						</input>
					</label>
					<label htmlFor='mark_5'>5
						<input type='radio' 
							   name='movie_rate' 
							   value='5' 
							   onClick={handler} 
							   id='mark_5'>
						</input>
					</label>
				</div>
				<div className='user_rating'>
					{
						state.mark !== undefined ? <div>Your mark: { state.mark }
														<img src={star} alt='star'></img>
													</div> : null
					}
				</div>
			</div>
		)
}

export default MovieGenres;