import React from 'react';
import { Link } from 'react-router-dom';

const toTop = () => {
	let clientHeight; 

	let scrollTop = ( ) => {
		clientHeight = window.pageYOffset; 
		if(clientHeight > 100) {
			window.scrollBy(0, -100);
		} else {
			clearInterval(top)
		}
	} 	
	let top = setInterval(scrollTop, 10)
}

const SimilarMovies = ({props}) => {
	return (
		<ul className='similar_movies'>
			{				
				props.map( (item, index) => {
					return (
						<Link key={ index } to={`/movie/${item.id}`} onClick={toTop}>
							<h4>{item.title}</h4>
							<img src={`https://image.tmdb.org/t/p/w154${item.poster_path}`} alt='movies_image'></img>
						</Link>
						)
				})			
			}
		</ul>
	)
}

export default SimilarMovies;