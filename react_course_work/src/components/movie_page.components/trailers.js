import React from 'react';

const Trailers = ( { props } ) => {
	return (
			<ul className='trailers_links'>
	 			<h3>Trailers:</h3>
	 			{
		 			props !== undefined ? 
		 				props.map(item => {
		 			  		return (
		 			  			<li key={item.id}>
		 			  				<a href={`https://www.youtube.com/watch?v=${item.key}`}>
		 			  					{item.name}
		 			  				</a>
		 			  			</li>
		 			  			)
		 			}) : (
		 			null)									
	 			}
	 		</ul>
		)
}

export default Trailers;