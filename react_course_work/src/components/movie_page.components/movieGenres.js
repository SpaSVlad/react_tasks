import React from 'react';
import { Link } from 'react-router-dom';

const MovieGenres = ( { props } ) => {
	return (
			<ul className='similar_genres'>
	 			<h3>Genres:</h3>
	 			{
		 			props !== undefined ? 
		 				props.map(item => {
		 			  		return (
		 			  			<li key={item.id}>
		 			  				<Link to={`/catalog/genres/${item.id}`}>
		 			  					{item.name}
		 			  				</Link>
		 			  			</li>
		 			  			)
		 			}) : (
		 			null)									
	 			}
	 		</ul>
		)
}

export default MovieGenres;