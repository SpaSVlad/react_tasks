import React from 'react';
import PropTypes from 'prop-types';

const Block = ({children, func, status, label, name}) => {
		return (
		  <div>
		  		<div>
	  				{label}
		  		</div>
				  	{
			  		  React.Children.map( children, (ChildrenItem) => {
			                return React.cloneElement(ChildrenItem, {
			                  ...ChildrenItem.props,
			                  active: ChildrenItem.props.value === status ? true : false,
			                  func: func,
			                  name : name
			                })
			              }
		        		)
				  	}
		 </div>
	)
}
Block.propTypes = {
	name : PropTypes.string.isRequired,
	func: PropTypes.func.isRequired,
	status: PropTypes.string,
	children: PropTypes.arrayOf(PropTypes.element)
}
export default Block;