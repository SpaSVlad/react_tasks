import React from 'react';

const button = ({value , active, func, name}) => {
	return (
		<button 
		name = {name}
		data-value = {value}
		style={{backgroundColor: active === true ? '#b6fff9' : 'transparent'}} 
		onClick ={func}> {value} </button>
	)
}
export default button;