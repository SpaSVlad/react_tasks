import React from 'react';

const button = ({value , active, func, name}) => {
	return (
		<button
		data-value={value}
		name={name}
		style={{backgroundColor: active === true ? '#b6fff9' : 'transparent'}} 
		onClick ={func}> {value} </button>
	)
}
export default button;