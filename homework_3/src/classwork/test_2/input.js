import React from 'react'; 
import PropTypes from 'prop-types';

const input = ({value, name, placeholder, handler, type, contentMaxLength}) => {
		return (
			<label>
				<div>{name}</div>
				<input
					name={name}
					placeholder={placeholder}
			     	value={value}
			     	type={type}
			      	onChange={handler}
			      	maxLength = {contentMaxLength}/>
			</label>
		)
	}

input.propTypes = {
	type: PropTypes.oneOf(['text', 'password', 'number']).isRequired,
    placeholder: PropTypes.string,
    value:  PropTypes.oneOfType([PropTypes.string, PropTypes.any]),
    handler: PropTypes.func.isRequired,
    contentLength: PropTypes.bool,
    contentMaxLength: PropTypes.number
}

export default input;