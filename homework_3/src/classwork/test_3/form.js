import React from 'react';
import Block from '../test_1/Block'
import GenderButton from '../test_1/genderButton'
import LayoutButton from '../test_1/layoutButton'
import Input from '../test_2/input'
import './form.css'

class Form extends React.Component {
  
  state = {
      activeGender: 'male',
      activeLayout:'left',
      name: '',
      password: '',
      age: '',
      language:''
  }


  smsFunc = (e) => {
    e.preventDefault()
    let name = e.target.name;
    let value = e.target.dataset.value;
      this.setState({
        [name]:value
      })

  }

  handler = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
      [name]: value
    })
    let contentLength = value.length;
    console.log(contentLength)
  }

  send = (e) => {
    e.preventDefault()
    console.log(this.state)
  }

  render = () => {
    const{activeGender, activeLayout, name, password, age, language} = this.state;
    const {smsFunc, handler, send} = this;

    return (
      <form onSubmit={send}>
          <Input 
              handler={handler}
              type='text'
              name='name'
              value={name}
              placeholder='Введите имя'
              contentMaxLength = {12}
          />
          <Input 
              handler={handler}
              type='password'
              name='password'
              value={password}
              placeholder='Введите пароль'
              contentMaxLength = {10}
          />
          <Block func={smsFunc} 
                 status={activeGender}
                 name ='activeGender'
                 label='Choose Gender'>
            <GenderButton value='male'/>
            <GenderButton value='female'/>
          </Block>
          <Input 
              handler={handler}
              type='number'
              name='age'
              value={age}
              placeholder='Введите свой возраст'
          />
          <Block func={smsFunc} 
                 status={activeLayout}
                 name='activeLayout'
                 label='Choose Layout'>
            <LayoutButton value='left'/>
            <LayoutButton value='center'/>
            <LayoutButton value='right' />
            <LayoutButton value='baseline' />
          </Block>
          <Input 
              handler={handler}
              type='text'
              name='language'
              value={language}
              placeholder='Любимый язык'
              contentMaxLength = {15}
          />
          <button>Submit</button>
        </form>
      )
  };
}

export default Form;
