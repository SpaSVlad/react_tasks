import React from 'react';



class buttonRef extends React.Component {

	ref = React.createRef()

	handler = () => {
		this.ref.current.classList.add('animated');
	}

	render = () => {
		const {handler} = this;
		return (
				<div className='parent'>
					<button ref={this.ref}
							onClick={handler}
							className='button'>
					Press me</button>
					</div>
			)
	}

}

export default buttonRef;