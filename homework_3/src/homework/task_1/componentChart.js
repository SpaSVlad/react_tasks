import React from 'react';
import Chart from 'chart.js';

class ChartComponent extends React.Component {

	chartRef = React.createRef();



	state = {
		data:[12, 10, 10, 20, 10, 0, 15]
	}
	
	chartEl = {};

	componentDidMount = () => {

		let chart = new Chart(this.chartRef.current, {
	      type: 'line',
	      data: {
	          labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
	          datasets: [{
	              label: 'My First dataset',
	              backgroundColor: 'rgb(255, 99, 132)',
	              borderColor: 'rgb(255, 99, 132)',
	              data: this.state.data
	          }]
	      },
	      options: {}
	    });
	    this.chartEl = chart;
	} 

	randomNum =(max) =>{
		  return Math.floor(Math.random() * Math.floor(max))
	}

	changeChart = () => {
		let randomArr = [];

		for (let i = 0 ; i < 7; i++) {
			randomArr.push(this.randomNum(99))
		}
	
		this.setState({
			data:randomArr
		})

	}
	
	componentDidUpdate = (prevProps, prevState, snapshot) => {
			this.chartEl.data.datasets[0].data = this.state.data;
			this.chartEl.update()
	}

	render = () => {
		const {changeChart} = this;
		return (
				<div>
					<canvas ref={this.chartRef}></canvas>
					<button onClick={changeChart}>Randomize Data</button>
				</div>
			)
	}
}

export default ChartComponent;