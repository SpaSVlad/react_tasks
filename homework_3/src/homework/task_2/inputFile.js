import React from 'react';

class Input extends React.Component {

	render = () => {
		const {handler, image} = this.props;
		return (
			<div className='imageChanger'>
			    <img src={image}></img>
				<label> Выбери картинку
					<input type='file' accept='image/*' onChange={handler}></input>
				</label>
			</div>
			)
	}
}

export default Input;