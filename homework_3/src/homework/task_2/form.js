import React from 'react';
import InputFile from './inputFile';
import CustomInput from '../../classwork/test_2/input';
import Block from '../../classwork/test_1/Block';
import GenderButton from '../../classwork/test_1/genderButton';


class Form extends React.Component {


	state = {
		image:'http://goodimg.ru/img/chernaya-kartinka2.jpg',
		name:'',
		activeGender: 'male'
	}

	handlesFiles = (e) => {
		const file = e.target.files[0];
		const reader = new FileReader();

		reader.readAsDataURL(file);

		reader.onload = () => this.setState({image:reader.result})
	}

	 toggler = (e) => {
	 	e.preventDefault()
	    let name = e.target.name;
	    let value = e.target.dataset.value;

	    this.setState({
	      [name]: value
	    })
	 }

	 saveInputValue = (e) => {
	    let name = e.target.name;
	    let value = e.target.value;

	    this.setState({
	      [name]: value
	    })
	 }



	render = () => {
		const {handlesFiles, saveInputValue, toggler} = this;
		const {image, name, activeGender} = this.state;

		console.log(this.state)
		return (
				<form>
					<CustomInput handler={saveInputValue}
					              type='text'
					              name='name'
					              value={name}
					              placeholder='Введите имя'
					              contentMaxLength = {12}/>
					<Block func={toggler} 
			                 status={activeGender}
			                 name ='activeGender'
			                 label='Choose Gender'>
				            <GenderButton value='male'/>
				            <GenderButton value='female'/>
			         </Block>
					<InputFile handler={handlesFiles} image={image}/>	
				</form>
			)
	}
}

export default Form;