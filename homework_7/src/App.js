import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import store from './redux/store';
import List from './components/list'
import User from './components/user'
import Post from './components/post'

import './App.css';


const App = () => {
  return (
    <BrowserRouter forceRefresh={true}>
      <Provider store={store}>
        <Route exact path='/' component={List}/>
        <Route exact path='/post/:postid' component={Post}/>
        <Route exact path='/user/:userid' component={User}/>

      </Provider>
    </BrowserRouter>
  )
}

export default App;

/* https://opencollective.com/core-js
 https://www.patreon.com/zloirock*/
