export const getPosts = (dispatch, getState) => {
	fetch('https://jsonplaceholder.typicode.com/posts/')
	.then(res => res.json())
	.then(res => {
		dispatch({
			type:'GET_POSTS',
			payload:res
		})
	})
	.catch( err => {
            dispatch({
                type: 'GET_POSTS_ERROR',
                payload: err
            });
		})
}

export const getPost = (num) => (dispatch, getState) => {
	fetch(`http://jsonplaceholder.typicode.com/posts/`)
	.then(res => res.json())
	.then(res => {
		res.map(item => {
			return item.id === Number(num) ? dispatch({type:'GET_POST',payload:item}) : null
		})
	})
	.catch( err => {
            dispatch({
                type: 'GET_POSTS_ERROR',
                payload: err
            });
		})
}

export const getUserPosts = (num) => (dispatch, getState) => {
	fetch(`http://jsonplaceholder.typicode.com/posts?userId=${num}`)
	.then(res => res.json())
	.then(res => {
		dispatch({
			type:'GET_USER_POSTS',
			payload:res
		})
	})
	.catch( err => {
            dispatch({
                type: 'GET_POSTS_ERROR',
                payload: err
            });
        })
}
