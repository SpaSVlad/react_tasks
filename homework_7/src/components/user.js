import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUserPosts } from '../actions/actions';

class User extends React.Component {


	componentDidMount = () => {
		const {handler, match} = this.props;
		handler(match.params.userid)
	}	

	render = () => {
		const {state} = this.props;
		return (
			<div>
			  <Link to='/' className='home_page'> Home Page </Link>
			  <ul className='user_list'>
			 	{
			 		state.data.map(item => {
			 			return (
				 				<li key={item.id}>
				 					<h3>{item.title}</h3>
				 					<p>{item.body}</p>
				 					<Link to={`/post/${item.id}`}> Show Post </Link>
				 				</li>
			 				)		
			 		})
			 	}
			  </ul>
		  </div>
		)	
	}
}
const mapStateToProps = ( state) => {
    return {
    	state: state.userPostsReducer
    }
}

const mapDispatchToProps = ( dispatch) => ({
    handler: (num) => {
    	dispatch( getUserPosts (num) )
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(User);