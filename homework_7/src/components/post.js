import React from 'react';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {getPost} from '../actions/actions';

class Post extends React.Component {

	componentDidMount = () => {
		const{ handler, match } = this.props;
		handler(match.params.postid)
	}

	render = () => {
	  	const { state, location, match} = this.props;
	  	return (
	  		<div className='post'>
	  			<Link to='/' className='home_page'> Home Page </Link>
		  		<ul className='post_list'>
		  			{	
		  				state.loaded ? (
			  				Object.entries(state.data).map(([key, value]) => {
			  					return <li key={key}>{key}:{value}</li>
			  				})
			  				) : 
		  					(
		  						<h2>Loading...</h2>
			  				)
		  			}
		  			<Link to={`/user/${state.data.userId}`} className='author_page'> Author </Link>
		  		</ul>
		  	</div>
		)
	}
}
const mapStateToProps = ( state) => {
    return {
    	state: state.postReducer
    }
}

const mapDispatchToProps = ( dispatch) => ({
    handler: (num) => {
    	dispatch( getPost (num) )
    }
});
export default connect(mapStateToProps, mapDispatchToProps)(Post);