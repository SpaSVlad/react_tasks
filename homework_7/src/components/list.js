import React from 'react';
import {connect} from 'react-redux';
import { Route, Link } from 'react-router-dom';
import {getPosts} from '../actions/actions';
import image from '../power.svg';

class List extends React.Component {

	state={
		count:25
	}

	componentDidMount = () => {
		const {handler} = this.props;
		handler()
	}

	showPosts = () => {
		this.setState({
			count: this.state.count+25
		})
	}

	render = () => {
		const { state } = this.props;
		const { showPosts } = this;
		const { count } = this.state;
		return (
				<div className='main'>
					<div className='main_title'>
						<img src='/static/media/power.af8d5eea.svg'></img>
						<h1>Posts</h1>
					</div>
					<ul className='main_list'>
						{	
							state.loaded ? (
								state.data.map(item => {
									return item.id <= count ? ( 
										<li key={item.id}>
											<Link to={`/post/${item.id}`}>
												{item.title}
											</Link>
										</li> 
										) : null
								})

							) :
							(<h2>Loading...</h2>)
						}
					</ul>
					<button onClick={showPosts}>Load more</button>
				</div>
			)
	}
}

const mapStateToProps = ( state) => {
    return {
    	state:state.postsReducer
    }
}

const mapDispatchToProps = ( dispatch) => ({
    handler: () => {
    	dispatch( getPosts )
    }
});
export default connect(mapStateToProps, mapDispatchToProps)(List);