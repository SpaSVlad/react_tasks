const initialState = {
	loaded:false,
	loading:false,
	error:false,
	data:[]
}

const userPostsReducer = (state=initialState, action) => {
	switch(action.type){
		case 'GET_USER_POSTS':
			return {
				...state,
				loaded:true,
				data:action.payload
			}
		case 'GET_POSTS_ERROR': 
			return {
				...state,
				error:true,
				data:{error:'error'}
			}
	default:
		return state;
	}
}

export default userPostsReducer;