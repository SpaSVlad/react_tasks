const initialState = {
	loaded:false,
	loading:false,
	error:false,
	data:[]
}

const postReducer = (state=initialState, action) => {
	switch(action.type){
		case 'GET_POST':
			return {
				...state,
				loaded:true,
				data:action.payload
			}
		case 'GET_POSTS_ERROR': 
			return {
				...state,
				error:true,
				data:{error:'error'}
			}
	default:
		return state;
	}
}

export default postReducer;