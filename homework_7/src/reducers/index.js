import {combineReducers} from 'redux';
import postsReducer from './postsReducer';  
import userPostsReducer from './userPosts'; 
import postReducer from './post'; 



const reducer = combineReducers({
	postsReducer,
	userPostsReducer,
	postReducer
});

export default reducer;