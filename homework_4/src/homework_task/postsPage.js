import React from 'react';
import { Route } from 'react-router-dom';
import RecentNews from './recentNews';
import singlePagePost from './singlePagePost';


class PostsPage extends React.Component {
	render = () => {
		return (
				<div>
            		<Route exact path='/posts' component={() => <h1>Post Page</h1>}/>
            		<Route exact path='/posts/:id' component={singlePagePost}/>
            		<Route exact path='/posts/limit/:id' component={RecentNews}/>
				</div>
        
			)
	}
}

export default PostsPage;