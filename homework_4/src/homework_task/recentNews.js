import React from 'react';

class RecentNews extends React.Component {

	state = {
		value:this.props.match.params.id,
		posts: []
	}

	inputValue = (e) => {
		let data = e.target.value;
		this.setState({
			value:data
		})
	}

	componentDidMount = () => {
		fetch('https://jsonplaceholder.typicode.com/posts/')
		.then(res => res.json())
		.then(res => {
			this.setState({
				posts: res
			})
		})
		console.log(this.state)
	}

	render = () => {
	const {inputValue} = this;
	const {posts, value} = this.state;
	const {match} = this.props;
	console.log(value)
	console.log(posts)
		return (
			<div>
				<h1>Recent News</h1>
				<input type='number' 
					   name='number'
				 	   placeholder='Сколько новостей показать?'
				 	   onChange={inputValue}>
				</input>
				<ul> 
					{
					   posts.map((post, key) => {
					   	return key <= Number(value-1) ? <li key={key}>
														   	<h2>{post.title}</h2>
														   	<p>{post.body}</p>
													   	</li> : null
					   }) 
					}
				</ul>
			</div>
			)
	}
}

export default RecentNews;

 {/*: posts.map((post, key) => {
						   	return key <= match.params.id-1 ? <li key={key}>
												   	<h2>{post.titlt}</h2>
												   	<p>{post.body}</p>
											   	</li> : null
						   }) */}

						   {/*!value === '' ?*/} 