import React from 'react';

class Post extends React.Component {

	state={
		posts:[],
		loadedPost: false,
		comments:[],
		showComm:false
	}

	componentDidMount = () => {
		fetch('https://jsonplaceholder.typicode.com/posts/')
		.then(res => res.json())
		.then(res => {

			this.setState({	
				posts:res,
				loadedPost:true
			})
		})

		fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
		.then(res => res.json())
		.then(res => {

			this.setState({
				comments: res
			})
		})

	}

	showComments = () => {
		this.setState({
			showComm:true
		})
	}


	render = () => {
	const {comments, showComm, loadedPost, posts} = this.state;
	const {location, match} = this.props;
	const {state} = location;
	const {showComments} = this;
		return (
				<div className='blockPost'>
					<div>
						{	state !== undefined ?

							posts.map( posts => {
								return posts.id === state.itemid ? <div key={posts.id}><h2>{posts.title}</h2><p>{posts.body}</p></div> : null
							}) : 

							posts.map(posts => {
					
								return posts.id === Number(match.params.id) ? <div key={posts.id}><h2>{posts.title}</h2><p>{posts.body}</p></div> : null
							})
						}
						<button onClick={showComments}>Показать коментарии</button>
					</div>
					<ul> 
						{ 	
							showComm ?
								state !== undefined ? 
									comments.map((item, key) => {
										return item.postId === state.itemid ? <li key={key}>{item.body}</li> 
										: null 
									}) 
									: 
									comments.map((item, key) => {
										return item.postId === Number(match.params.id) ? <li key={key}>{item.body}</li> 
										: null
									}) 
								: null
						}
					</ul>
				</div>
			)
	}
}

export default Post;