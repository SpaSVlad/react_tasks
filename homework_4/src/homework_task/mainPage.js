import React from 'react';
import { Route, Link} from 'react-router-dom';


class MainPage extends React.Component {

	state ={
		dataUsers:[],
		loadedUsers: [],
		counter: 20,
		loaded:false
	}

	showUsers = () => {
		const {dataUsers, counter, loadedUsers} = this.state;

		let addCounter = counter+20;
		let addUsers = dataUsers.slice(0,addCounter);

		this.setState({
			loadedUsers:addUsers,
			counter: addCounter

		})
	}

	componentDidMount = () => {
		fetch('https://jsonplaceholder.typicode.com/posts')
		.then(res => res.json())
		.then(res => {
			this.setState({
				dataUsers:res,
				loadedUsers:res.slice(0,20),
				loaded:true
			})
		})
	}

	render = () => {
		const{loadedUsers, loaded} = this.state;
		const {showUsers} = this;

		return (
				<div>
					<h1>Main Page</h1>
					<ul>
						{!loaded ? <div>...Loading</div> :
							loadedUsers.map(item => {
								return (<li key={item.id}>
											<Link to={{pathname:`/posts/${item.id}`, state:{itemid:item.id}}}>
												{item.title}
											</Link>
										</li>
								)
					
							})
						}
					</ul>
					<button onClick={showUsers}>Показать еще</button>
				</div>


			)
	}
}

export default MainPage;