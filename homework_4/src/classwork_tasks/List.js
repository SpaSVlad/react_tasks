import React from 'react';
import UsersList from './usersList';
import Item from './Item';
import { Route, Link } from 'react-router-dom';

const List = () => {
		return (	
		  <div>
	          <Route exact path="/list" component={UsersList}/>
	          <Route path='/list/:itemid' component={Item}/>
          </div>
		)
}

export default List;
