import React from 'react';
import { Link } from 'react-router-dom';

class UserList extends React.Component {

	state={
		users: []
	}

	componentDidMount = () => {
		let data = localStorage.getItem('data');

		if (data !== null) {
			console.log(JSON.parse(data))
			this.setState({
				users:[
					...JSON.parse(data)
				]	
			})	
		} else {
			fetch('https://jsonplaceholder.typicode.com/users')
			.then(res => res.json())
			.then(res => {
				this.setState({
					users: res
				})
			})
		}
	}

	render = () => {
		const {users} = this.state;
		return (
			<ul>
				{
					users.map(item => {
						return <li key={item.id} className='user'> 
									<Link to={{pathname:`/list/${item.id}`,
											  state: users}}> 
											  {item.name}
											  <span>Rating: {item.rating === undefined ? '0' : item.rating}</span>
									</Link>
								</li>
					})

				}
			</ul>
			)
	}
}

export default UserList;