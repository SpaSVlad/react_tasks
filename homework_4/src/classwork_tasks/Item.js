import React from 'react';

class Item extends React.Component {

	state = {
		users: this.props.location.state
	}

	setRating = (e) => {
		const {match} = this.props;
		const {users} = this.state;

		const data = users.map(item => {
			if (item.id === Number(match.params.itemid)) {
				item.rating = e.target.value;
				return item
			}
			return item
		})

		this.setState({
			users:[
				...data
			]
		})
		this.toLocalStorage()
	}

	toLocalStorage = () => {
		localStorage.setItem('data', JSON.stringify(this.state.users))
	}

	componentDidMount = () => {
		let data = localStorage.getItem('data');
		if (data !== null) {
			console.log(JSON.parse(data))
			this.setState({
				users:[
					...JSON.parse(data)
				]	
			})	
		} 
	}

	render = () => {
	const { setRating } = this;
	const { users } = this.state;
	const { match } = this.props;
		return (
			<div>
				<div className='mark'>
					<label>
						<span>1</span>
						<input type='radio' onChange={setRating} value='1' name='setRaiting'></input>
					</label>
					<label>
						<span>2</span>
						<input type='radio' onChange={setRating} value='2' name='setRaiting'></input>
					</label>
					<label>
						<span>3</span>
						<input type='radio' onChange={setRating} value='3' name='setRaiting'></input>
					</label>
					<label>
						<span>4</span>
						<input type='radio' onChange={setRating} value='4' name='setRaiting'></input>
					</label>
					<label>
						<span>5</span>
						<input type='radio' onChange={setRating} value='5' name='setRaiting'></input>
					</label>
				</div>
					<ul> 
						{
							users.map(item => {
								return item.id !== Number(match.params.itemid) ? null : (
									<div key={item.id}>
										<li><b>{item.name}</b></li>
										<li>Raiting: {item.rating === undefined ? '0' : item.rating}</li>
										<li>{item.email}</li>
										<li>{item.username}</li>
										<li>{item.phone}</li>
										<li>{item.address.city}, {item.address.street}, {item.address.suite}</li>
									</div>
								)	
							})
						}
					</ul>
				</div>
		)		
	}
}

export default Item;