import React from 'react';
import { BrowserRouter, Route, Switch, Link} from 'react-router-dom';

import HomePage from './classwork_tasks/HomePage';
import List from './classwork_tasks/List';
import Contacts from './classwork_tasks/Contacts';
import About from './classwork_tasks/About';
import Error from './classwork_tasks/error404'

import MainPage from './homework_task/mainPage';
import PostsPage from './homework_task/postsPage';


class App extends React.Component {

  render = () => {
    return (
        <BrowserRouter forceRefresh={true}>
        {/*classwork_tasks*/}
          {/*<div>
            <div> 
              <Link to="/"> HomePage </Link>
              <Link to="/list"> List </Link> 
              <Link to="/contacts"> Contacts </Link>
              <Link to="/about"> About </Link>
            </div>

            <Switch>
              <Route exact path="/" component={HomePage}/>
              <Route path="/list" component={List}/>
              <Route exact path="/contacts" component={Contacts}/>
              <Route exact path="/about" component={About}/>
              <Route component={Error}/>
            </Switch>

          </div>*/}
  
        {/*homework_task*/}
          <div>
              <div className='menu'>
                <Link exact to='/'> Main </Link>
                <Link exact to='/posts'> Posts </Link>
                <Link exact to='/posts/limit/:id'> Recent News </Link>
                <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Logo_TV_2015.svg/1200px-Logo_TV_2015.svg.png'></img>
              </div>

              <Switch>
                <Route exact path='/' component={MainPage}/>
                <Route path='/posts/' component={PostsPage}/>
                <Route component={() => <h1> Not Found </h1>}/>
              </Switch>
          </div>
      </BrowserRouter>
      )
  }
}

export default App;
