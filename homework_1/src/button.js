import React from 'react';
const buttonName = 'Прибыл';

const buttonStyle = {
	backgroundColor:'#75ba1a',
	border:'none',
	borderRadius:'3px',
	fontWeight:'bold',
	padding:'5px 10px'
}

const Button =({func, index}) => {
	return (
		<button onClick={func} style={buttonStyle} data-index={index}>{buttonName}</button>
	)
}
export default Button;