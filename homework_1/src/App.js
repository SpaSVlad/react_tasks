import React from 'react';
import guests from './guests.json'
import Button from './button'
import Input from './input'



const modifieldArr = guests.map(item => {
	  item.arrived = false
	  return item;
})
const formStyle = {
	padding:'0px 30px',
	width:'350px',
	margin:'auto',
	border: '20px solid #f3f3f3'
}
const divStyle = {
	display:'flex',
	justifyContent:'space-between',
	alignItems: 'center'
}

class Guest extends React.Component {

	state = {
		data:modifieldArr
	}
	checkGuest = (e) => {
		e.preventDefault();
		const guestIndex = +e.target.dataset.index;
		let arrivedGuests = this.state.data.map(elem => {
				if (guestIndex === elem.index) {
					elem.arrived = !elem.arrived
				}
			return elem;
		});

		this.setState({
			data:arrivedGuests
		});
	}
	searchGuest = (e) => {
		let inputValue = e.target.value.toUpperCase();
		const neededGuest = this.state.data.filter(item => {
			console.log(item)
				for(let key in item) {
					if (item[key].toString().toUpperCase().indexOf(inputValue, 0)>=0) {
						return item
					}
				}
		});
			console.log(neededGuest)
		if (inputValue === '') {
			this.setState({
				data:modifieldArr
			});	
		} else {
			this.setState({
				data:neededGuest
			});	
		}
	}
	render = () => {
		const {checkGuest} = this;
		const {searchGuest} = this;
		const {data} = this.state;

		return (
			<form style ={formStyle}> 
				<div style={divStyle}>
					<h1>Список гостей</h1>
					<div style={{width:'50px', height:'50px', backgroundColor:'yellow'}}></div>
				</div>
				<Input func = {searchGuest}/>
				{
				data.length > 0 ? <ul style={{padding:'0px'}}>
					{
						data.map((item, key) => {
							 return (
								 <li  
									 key={key}
									 style={{textDecoration: item.arrived ? 'line-through' : 'none', 
											 listStyle:'none', 
											 padding:'5px', 
											 display:'flex',
											 justifyContent:'space-between',
											 alignItems:'flex-start',
											 fontSize: '12px',
											 background: '#f1f1f161'
											}
									}> 
									 <p>Гость <b>{item.name}</b> работает в компании <b>"{item.company}".</b><br/>
										 Его контакты: <b>{item.phone}</b>;
										 <b>{item.address}</b>
									 </p>
									 <Button func={checkGuest} index = {item.index}/>
								 </li>
							 )
						})
					}
					</ul> : <div>Совпадений не найдено</div>
				}
			</form>
		)
	};
}


export default Guest;