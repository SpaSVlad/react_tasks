import React from 'react';

const inputStyle = {
	width:'96%',
	padding: '10px 0px 10px 10px',
	border:'none',
	borderRadius:'3px',
	backgroundColor:'#f3f3f3'
}

const Input = ({func}) => {
	return (
		<input placeholder='Введите имя гостя для поиска' onInput={func} style={inputStyle}></input>
	)
}
export default Input;