const initState = {
	tasks: [{name:'eat', done:false, id:1}, 
			{name:'sleep', done:false, id:2}, 
			{name:'sit', done:false, id:3}, 
			{name:'run', done:false, id:4},
			{name:'drive', done:false, id:5}]
}

const reducer = (state = initState, action) => {
    switch( action.type ){
        case 'DONE':
            return {
                ...state,
                tasks: action.payload
            }
        
        case 'REMOVE':
            return{
                ...state,
                tasks: action.payload
            }

        case 'ADD':
            return{
                ...state,
                tasks:[...state.tasks, action.payload]
            }

        default:
            return state;
    }
    return state;
}

export default reducer;