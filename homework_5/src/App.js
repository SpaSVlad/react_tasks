import React from 'react';
import { BrowserRouter, Route, Link} from 'react-router-dom';
import { Provider } from 'react-redux';


import List from './components/list';
import AllTasks from './components/allTaks';
import UndoneTask from './components/undoneTask';
import DoneTask from './components/doneTask';
import store from './redux/store';

class App extends React.Component {
  render = () => {
    return (
      <BrowserRouter>
          <Provider store={store}>
              <Link to='/'> Main </Link>
              <Link to='/done'> Done </Link>
              <Link to='/undone'> Undone </Link>

              <Route exact path='/' component={AllTasks}/>
              <Route exact path='/done' component={DoneTask}/>
              <Route exact path='/undone' component={UndoneTask}/>
          </Provider>
      </BrowserRouter>
    );
  }
}

export default App;
