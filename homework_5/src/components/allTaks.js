import React from 'react';
import List from './list'
import { connect } from 'react-redux';

class AllTasks extends React.Component {

	state = {
		newTask: {
			name:'',
			done:false,
			id:''
		}
	}

	addTask = (e) => {
		const {newTask} = this.state;

		let inputValue = e.target.value;

		this.setState({
			newTask:{...newTask, name:inputValue, id: new Date().getTime()}
		})
	}

	addTaskToList = () => {
		const {addTaskToStore} = this.props;
		const {newTask} = this.state;

		if (newTask.name !== '' && newTask.id !== '') {
			addTaskToStore(newTask)
		}
		this.setState({
			newTask: { 
				name:'',
				id:'',
				done:false
			}
		})
	}

	render = () => {
		const {tasks} = this.props;
		const {addTask, addTaskToList} = this;
		return (
				<div>
					<h1>List</h1>
					<input onChange={addTask}></input>
					<button onClick={addTaskToList}>Add Task</button>
					<List/>
				</div>
		)
	}
}

const mapStateToProps = ( state ) => {
	return {
		tasks:state.tasks
	}
}

const mapDispatchToProps = ( dispatch ) => ({
	
		addTaskToStore: (task) => {
			dispatch({
				type:'ADD',
				payload: task
			})
		}
})

export default connect(mapStateToProps, mapDispatchToProps)(AllTasks);