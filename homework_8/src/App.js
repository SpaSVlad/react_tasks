import React from 'react';
import { BrowserRouter, Route, Switch, Link} from 'react-router-dom';
import { Provider } from 'react-redux';

import HelmetWrapper from './components/helmetWrapper';
import Main from './components/mainPage';
import Posts from './components/posts';
import store from './redux/store';
import './App.css';

const wrappedMain = HelmetWrapper({title:'MAIN'})(Main);
const wrappedPosts = HelmetWrapper({title:'POSTS'})(Posts);

const App = () => {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <Link to='/'>Main</Link>
        <Link to='/posts'>Posts</Link>
        <Switch>
          <Route exact path='/' component={wrappedMain}/>
          <Route exact path='/posts' component={wrappedPosts}/>
          <Route component={() => (<h1>Not Found</h1>)}/>
        </Switch>
      </Provider>
    </BrowserRouter>
  );
}

export default App;
