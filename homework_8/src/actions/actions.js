import axios from '../helpers/axios';

export const getPosts = () => (dispatch, getState) => {
	axios.get('/posts')
	.then(res => {
		dispatch({
			type:'GET_POSTS',
			payload:res.data
		})
	})
}

export const removePost = (id) => (dispatch, getState) => {
	let postsData = getState().state.posts.filter(item => {
		if ( item.id !== Number(id) ) {
			return item
		}
	})

	dispatch({
		type:'REMOVE_POST',
		payload:postsData
	})
}	

