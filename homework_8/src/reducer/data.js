
const initState = {
	posts:[]
}

const reducer = (state = initState, action) => {
	switch(action.type){
		case 'GET_POSTS':
		 return {
		 	state:{
		 		posts:[...action.payload]
		 	}
		}

		case 'REMOVE_POST':
		  return {
		 	state:{
		 		posts:[...action.payload]
		 	}
		}

		default: 
		 	return {
		 		state
		 	}
	}
}

export default reducer;