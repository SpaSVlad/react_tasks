import React from 'react';
import Helmet from 'react-helmet';
import favicon from '../images/favicon_smile.ico?v2';

const withHelmet = ({title}) => (Component) => {
	return () => (
		<div>
			<Helmet>
				<title>{title}</title>
				<link rel="shortcut icon" href={favicon}/>
			</Helmet>
			<>	
				<h1>{title}</h1>
				<Component/>
			</>
		</div>
	)
}

export default withHelmet;