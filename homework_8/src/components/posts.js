import React from 'react';
import withHelmet from './helmetWrapper';
import { getPosts, removePost } from '../actions/actions';
import { connect } from 'react-redux';


class Posts extends React.Component {

	componentDidMount = () => {
		this.props.getAllPosts()
	}

	remPost = (e) => {
		this.props.remove_post( e.target.dataset.id )
	}

	render = () => {
		const { state } = this.props;
		const { remPost } = this;
		return (
				<div>
					<ul>
						{
							state.map(item => {
								return <li key={item.id} onClick={remPost} data-id={item.id}>{item.title}</li>
							})
						}
					</ul>
				</div>
			)
	}
}

const mapStateToProps = ({state}) => {
	return({
		state:state.posts
	})
}
const mapDispatchToProps = (dispatch) => ({
	getAllPosts: () => { 
		dispatch( getPosts() )
	},
	remove_post: (id) => {
		dispatch( removePost ( id ))
	}
	
})


export default connect(mapStateToProps, mapDispatchToProps )(Posts)