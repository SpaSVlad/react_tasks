import axios from 'axios';

const axios_instance = axios.create({
	baseURL:'https://jsonplaceholder.typicode.com'
})

export default axios_instance;